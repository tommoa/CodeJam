#include <cstdint>
#include <iostream>
#include <array>
#include <cmath>

using namespace std;

array<uint64_t, 2> bathroom(uint64_t k, uint64_t n) {
    uint64_t a = k >> 1;
    array<uint64_t, 2> ret;
    cout << k << ", " << n << endl;
    if (n == 1) {
	if (k&1) {
	    ret[0] = a;
	    ret[1] = a;
	    return ret;
	}
	ret[0] = a;
	ret[1] = a-1;
	return ret;
    }
    if (n == 2)
	return bathroom(k >> 1, 1);
    if (n == 3) {
	if (k&1)
	    return bathroom((k >> 1), 1);
	return bathroom((k >> 1) -1, 1);
    }
    int downpower2 = (int)log2(k);
    if ( n >= (1 << downpower2) || k - n < (1 << (downpower2-1))) {
	cout << "straight to 0, 0" << endl;
	ret[0] = 0;
	ret[1] = 0;
	return ret;
    }
    //*
    if (n >= (1 << (downpower2-1)) && n < (1 << (downpower2))) {
	cout << "straight to 1, 0 from ";
	if (n >= (1 << (downpower2-1))) 
	    cout << "first term" << endl;
	else
	    cout << "second term" << endl;
	ret[0] = 1;
	if ( (1<<(downpower2+1)) -1 == k)
	    ret[1] = 1;
	else
	    ret[1] = 0;
	return ret;
    }//*/
    int d = (1 << (downpower2 - 1));
    d = (int)log2(k-d) + 1;
    ret = bathroom((k-d), n);
    if (ret[0] == ret[1]) {
	ret[0] += 1;
	cout << "increased to " << ret[0] << ", " << ret[1] << endl;
	return ret;
    }
    	cout << "increased to " << ret[0] << ", " << ret[1] + 1 << endl;
	ret[1] += 1;
	return ret;
}

int main()
{
    int numCases = 0;
    cin >> numCases;

    for (int q = 0; q < numCases; q++)
    {
	uint64_t k = 0;
	cin >> k;

	uint64_t n = 0;
	cin >> n;

	uint64_t highest = 0;

	uint64_t lowest = 0;

	auto thing = bathroom(k, n);

	highest = thing[0];
	lowest = thing[1];
	
	cout << "Case #" << q+1 << ": " << highest << " " << lowest << endl;
    }
    return 0;
}
