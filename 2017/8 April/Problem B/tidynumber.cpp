#include <iostream>
#include <cstdint>
#include <string>

using namespace std;

int64_t tidy(int64_t input)
{
    for (int64_t i = input; i > 0; i--) {
	if (i < 10)
	    return i;
	bool ret = false;
        int last_digit = 10;
	for (int64_t j = i; j != 0; j /= 10) {
	    int d = j %10;
	    if (d > last_digit) {
		ret = false;
		break;
	    }
	    last_digit = d;
	    ret = true;
	}
	if (ret)
	    return i;
    }
}
	    

int main()
{
    int numCases = 0;
    cin >> numCases;

    for (int q = 0; q < numCases; q++)
    {
	int64_t n = 0;
	cin >> n;

	int64_t highest = 0;
	highest = tidy(n);
	cout << "Case #" << q+1 << ": " << highest << endl;
    }
    return 0;
}
