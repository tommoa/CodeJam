#include <iostream>
#include <vector>

using namespace std;

void drawGrid(vector<vector<char>>& grid) {
    for (const auto& v: grid) {
	for (char c: v) {
	    cout << c << ' ';
	}
	cout << endl;
    }
}

struct Model {
    char type;
    int row;
    int column;
};

int calculateScore(vector<vector<char>>& grid) {
    int ret = 0;
    for (const auto& v: grid) {
	for (char c: v) {
	    switch (c) {
	    case '.':
		break;
	    case 'o':
		ret += 2;
		break;
	    case 'x':
	    case '+':
		ret += 1;
		break;
	    default:
		break;
	    }
	}
    }
    return ret;
}

bool isLegalRow(vector<vector<char>>& grid, int rowNum) {
    bool lastWasOut = false;
    for (int i = 0; i < grid.size(); i++) {
	char c = grid[rowNum][i];
	switch (c) {
	case '.':
	    continue;
	case 'x':
	case 'o':
	    if (lastWasOut)
		return false;
	    lastWasOut = true;
	    break;
	case '+':
	    lastWasOut = false;
	}
    }
    return true;
}

bool isLegalColumn(vector<vector<char>>& grid, int columnNum) {
    bool lastWasOut = false;
    for (int i = 0; i < grid.size(); i++) {
       char c = grid[i][columnNum];
	switch (c) {
	case '.':
	    continue;
	case 'x':
	case 'o':
	    if (lastWasOut)
		return false;
	    lastWasOut = true;
	    break;
	case '+':
	    lastWasOut = false;
	}
    }
    return true;
}

bool isLegalDiagonal(vector<vector<char>>& grid, int columnNum, int rowNum) {
    bool lastWasOut = false;
    int c = columnNum + rowNum;
    for (int i = 0; i <= c; i++) {
	switch(grid[i][c-i]) {
	case '.':
	    continue;
	case '+':
	case 'o':
	    if (lastWasOut)
		return false;
	    lastWasOut = true;
	    break;
	case 'x':
	    lastWasOut = false;
	}
    }
    int c = 
    return true;

void doStuff(vector<vector<char>>& grid) {
    int score = calculateScore(grid);
    
}

int main() {
    int numCases = 0;
    cin >> numCases;

    for ( int q = 0; q < numCases; q++ ) {
	int n = 0;
	int m = 0;

	cin >> n;
	cin >> m;

	vector<vector<char>> grid(n);

	for (auto& v: grid) {
	    v.resize(n, '.');
	}

	for (int i = 0; i < m; ++i) {
	    Model model;

	    cin >> model.type >> model.row >> model.column;

	    grid[model.row - 1][model.column - 1] = model.type;
	}

	drawGrid(grid);

	doStuff(grid);
    }
}
